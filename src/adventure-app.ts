import { FirebaseService, Key } from "./services/firebase-service";
import { OtherPlayerCharacter, PlayerCharacter } from "./classes/character";
import { Board } from "./classes/board";
import { ChatService } from "./services/chat-service";
import { Images } from "./helpers/image-helper";

// Realtime Database
const firebaseService = FirebaseService.instance;

// Authorization
const bodyElement = document.getElementsByTagName("body")[0] as HTMLBodyElement;
const chatElement = document.getElementById("chatContainer");
const statsElement = document.getElementById("statsContainer");
const loginScreen = document.getElementById("loginScreen");
const usernameField = document.getElementById("username") as HTMLInputElement;
const emailField = document.getElementById("email") as HTMLInputElement;
const passwordField = document.getElementById("password") as HTMLInputElement;
const repeatPasswordField = document.getElementById("repeatPassword") as HTMLInputElement;
const loginButton = document.getElementById("loginButton") as HTMLButtonElement;
const errorMessageField = document.getElementById("errorMessage");

loginButton.onclick = () => login();
passwordField.onkeyup = repeatPasswordField.onkeyup = (event: KeyboardEvent) => {
  if (event.key === "Enter") {
    passwordField.blur();
    loginButton.click();
  }
};

document.getElementById("loginToggle").addEventListener("click", (event: Event) => {
  if (usernameField.classList.contains("displayNone")) {
    usernameField.classList.remove("displayNone");
    repeatPasswordField.classList.remove("displayNone");
    loginButton.value = "Sign Up";
    loginButton.onclick = () => signUp();
    (event.target as HTMLElement).innerHTML = "Login";
  } else {
    usernameField.classList.add("displayNone");
    repeatPasswordField.classList.add("displayNone");
    loginButton.value = "Login";
    loginButton.onclick = () => login();
    (event.target as HTMLElement).innerHTML = "Sign Up";
  }
});

function signUp() {
  firebaseService
    .signUp(emailField.value, passwordField.value, repeatPasswordField.value, usernameField.value)
    .then(loginCallback)
    .catch((error: Error) => {
      errorMessageField.innerHTML = error.message;
    });
}

function login() {
  firebaseService
    .signIn(emailField.value, passwordField.value)
    .then(loginCallback)
    .catch(() => {
      errorMessageField.innerHTML = "Invalid Email or Password";
    });
}

const loginCallback = () => {
  chatElement.style.visibility = statsElement.style.visibility = "visible";
  loginScreen.remove();
  let playerCharacter: PlayerCharacter;
  const chatService = ChatService.instance;
  const otherPlayerCharacters: { [playerName: string]: OtherPlayerCharacter } = {};

  firebaseService.getCurrentPlayerPositions().then((snapshot) => {
    if (snapshot.val()) {
      const boardPosition = snapshot.val()[Key.boardPosition];
      const characterPosition = snapshot.val()[Key.charPosition];
      Board.init(boardPosition[Key.boardPosX], boardPosition[Key.boardPosY]);
      playerCharacter = PlayerCharacter.init(characterPosition[Key.charPosX], characterPosition[Key.charPosY]);
    } else {
      Board.init(9, 9);
      playerCharacter = PlayerCharacter.init(9, 9);
    }
  });

  // Firebase Listeners
  firebaseService.addOnChildAddedListener(Key.currentlyOnline, (snapshot) => {
    if (snapshot.key == firebaseService.currentUserName) return;
    firebaseService.getPlayerPositions(snapshot.key).then((positions) => {
      if (positions.val()) {
        otherPlayerCharacters[snapshot.key] = new OtherPlayerCharacter(
          snapshot.key,
          positions.val()[Key.charPosition][Key.charPosX],
          positions.val()[Key.charPosition][Key.charPosY],
          positions.val()[Key.boardPosition][Key.boardPosX],
          positions.val()[Key.boardPosition][Key.boardPosY],
        );
      } else {
        otherPlayerCharacters[snapshot.key] = new OtherPlayerCharacter(snapshot.key, 9, 9, 9, 9);
      }
    });
    chatService.printMessage(snapshot.key + " is online", "green");
  });

  firebaseService.addOnChildRemovedListener(Key.currentlyOnline, (snapshot) => {
    if (snapshot.key == firebaseService.currentUserName) return;
    otherPlayerCharacters[snapshot.key].removeElementFromBoard();
    delete otherPlayerCharacters[snapshot.key];
    chatService.printMessage(snapshot.key + " is offline", "red");
  });

  firebaseService.addOnChildAddedListener(`${Key.flaggingPlayers}`, (snapshot) => {
    if (snapshot.key == firebaseService.currentUserName) {
      chatService.printMessage("You are looking for a duel...", "blue");
    } else {
      chatService.printMessage(snapshot.key + " is looking for a duel!", "blue");
      if (otherPlayerCharacters[snapshot.key]) {
        otherPlayerCharacters[snapshot.key].imageElement.src = Images.character.duel.src;
      } else {
        setTimeout(() => {
          if (otherPlayerCharacters[snapshot.key])
            otherPlayerCharacters[snapshot.key].imageElement.src = Images.character.duel.src;
        }, 200);
      }
    }
  });

  firebaseService.addOnChildRemovedListener(`${Key.flaggingPlayers}`, (snapshot) => {
    if (snapshot.key == firebaseService.currentUserName) return;
    if (otherPlayerCharacters[snapshot.key])
      otherPlayerCharacters[snapshot.key].imageElement.src = Images.character.base.src;
  });

  // Character Movement
  let upIsPressed = false;
  let downIsPressed = false;
  let leftIsPressed = false;
  let rightIsPressed = false;
  let upBlocked = false;
  let downBlocked = false;
  let leftBlocked = false;
  let rightBlocked = false;

  const moveUpLoop = () => {
    if (upBlocked) return;
    upBlocked = true;

    setTimeout(() => {
      upBlocked = false;
      if (upIsPressed) {
        moveUpLoop();
      }
    }, 300);

    if (downIsPressed) return;
    playerCharacter.moveUp();
  };

  const moveDownLoop = () => {
    if (downBlocked) return;
    downBlocked = true;

    setTimeout(() => {
      downBlocked = false;
      if (downIsPressed) {
        moveDownLoop();
      }
    }, 300);

    if (upIsPressed) return;
    playerCharacter.moveDown();
  };

  const moveLeftLoop = () => {
    if (leftBlocked) return;
    leftBlocked = true;

    setTimeout(() => {
      leftBlocked = false;
      if (leftIsPressed) {
        moveLeftLoop();
      }
    }, 300);

    if (rightIsPressed) return;
    playerCharacter.moveLeft();
  };

  const moveRightLoop = () => {
    if (rightBlocked) return;
    rightBlocked = true;

    setTimeout(() => {
      rightBlocked = false;
      if (rightIsPressed) {
        moveRightLoop();
      }
    }, 300);

    if (leftIsPressed) return;
    playerCharacter.moveRight();
  };

  // Key Listeners
  bodyElement.addEventListener("keydown", (event: KeyboardEvent) => {
    switch (event.key) {
      case "ArrowUp": {
        if (upIsPressed) return;
        upIsPressed = true;
        moveUpLoop();
        break;
      }

      case "ArrowDown": {
        if (downIsPressed) return;
        downIsPressed = true;
        moveDownLoop();
        break;
      }

      case "ArrowLeft": {
        if (leftIsPressed) return;
        leftIsPressed = true;
        moveLeftLoop();
        break;
      }

      case "ArrowRight": {
        if (rightIsPressed) return;
        rightIsPressed = true;
        moveRightLoop();
        break;
      }
    }
  });

  bodyElement.addEventListener("keyup", (event: KeyboardEvent) => {
    switch (event.key) {
      case "ArrowUp": {
        upIsPressed = false;
        break;
      }
      case "ArrowDown": {
        downIsPressed = false;
        break;
      }
      case "ArrowLeft": {
        leftIsPressed = false;
        break;
      }
      case "ArrowRight": {
        rightIsPressed = false;
        break;
      }
      case "Enter": {
        chatService.focusChat();
      }
    }
  });
};

//Preload Images
Images.init();
