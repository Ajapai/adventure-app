import { Images } from "../helpers/image-helper";
import { RandomHelper } from "../helpers/random-helper";

export abstract class Ability {
  readonly animationSrc: string;
  protected readonly _level: number;

  abstract readonly name: string;
  abstract readonly type: AbilitiyType;
  abstract readonly targetsEnemy: boolean;
  abstract readonly damageDelay: number;
  abstract get manaCost(): number;
  abstract get description(): string;

  protected abstract get minDamage(): number;
  protected abstract get maxDamage(): number;

  constructor(animationSrc: string, level: number) {
    this.animationSrc = animationSrc;
    this._level = level;
  }

  protected get damageRange() {
    return `${Math.abs(this.minDamage)} - ${Math.abs(this.maxDamage)}`;
  }

  get damage() {
    return RandomHelper.randomRange(this.minDamage, this.maxDamage);
  }

  get tooltip() {
    return `MP Cost: ${this.manaCost}\n${this.description}`;
  }

  static create(abilityType: AbilitiyType, level: number) {
    switch (abilityType) {
      case AbilitiyType.Empty: {
        return null;
      }
      case AbilitiyType.Attack: {
        return new Attack(level);
      }
      case AbilitiyType.Flame: {
        return new Flame(level);
      }
      case AbilitiyType.Heal: {
        return new Heal(level);
      }
      case AbilitiyType.RockThrow: {
        return new RockThrow(level);
      }
      case AbilitiyType.LeafCutter: {
        return new LeafCutter(level);
      }
    }
  }
}

class Attack extends Ability {
  readonly name = "Attack";
  readonly type = AbilitiyType.Attack;
  readonly manaCost = 0;
  readonly targetsEnemy = true;
  readonly damageDelay = 0;
  get description(): string {
    return `A normal attack that deals '${this.damageRange}' damage`;
  }

  protected get minDamage() {
    return this._level * 3;
  }
  protected get maxDamage() {
    return this._level * 4;
  }

  constructor(level: number) {
    super(Images.attack.src, level);
  }
}

class RockThrow extends Ability {
  readonly name = "Rock Throw";
  readonly type = AbilitiyType.RockThrow;
  readonly targetsEnemy = true;
  readonly damageDelay = 333;
  get manaCost(): number {
    return 2 + Math.ceil(this._level / 2);
  }
  get description(): string {
    return `A thrown rock with high damage variance, dealing anywhere between '${this.damageRange}' damage`;
  }
  protected get minDamage(): number {
    return this._level;
  }
  protected get maxDamage(): number {
    return this._level * 8;
  }
  constructor(level: number) {
    super(Images.rockThrow.src, level);
  }
}

class Flame extends Ability {
  readonly name = "Flame";
  readonly type = AbilitiyType.Flame;
  readonly targetsEnemy = true;
  readonly damageDelay = 100;
  get manaCost() {
    return 4 + this._level;
  }
  get description(): string {
    return `A small flame that deals '${this.damageRange}' damage`;
  }
  protected get minDamage(): number {
    return Math.ceil((2 + this._level) * 2.75);
  }
  protected get maxDamage(): number {
    return Math.floor((2 + this._level) * 4.75);
  }

  constructor(level: number) {
    super(Images.flame.src, level);
  }
}

class Heal extends Ability {
  readonly name = "Heal";
  readonly type = AbilitiyType.Heal;
  readonly targetsEnemy: false;
  readonly damageDelay = 333;
  get manaCost(): number {
    return this._level * 2 + 6;
  }
  get description(): string {
    return `A possibly strong heal that can restore '${this.damageRange}' health`;
  }
  protected get minDamage(): number {
    return -Math.ceil((4 + this._level) * 1.75);
  }
  protected get maxDamage(): number {
    return -Math.floor((4 + this._level) * 5.75);
  }

  constructor(level: number) {
    super(Images.heal.src, level);
  }
}

class LeafCutter extends Ability {
  name = "Leaf Cutter";
  type = AbilitiyType.LeafCutter;
  targetsEnemy = true;
  damageDelay = 200;
  get manaCost(): number {
    return 0;
  }
  get description(): string {
    return "None";
  }
  protected get minDamage() {
    return this._level * 4;
  }
  protected get maxDamage() {
    return this._level * 5;
  }
  constructor(level: number) {
    super(Images.leafCutter.src, level);
  }
}

export enum AbilitiyType {
  Empty = "Empty",
  Attack = "Attack",
  RockThrow = "RockThrow",
  Flame = "Flame",
  Heal = "Heal",
  LeafCutter = "LeafCutter",
}
