import { CharacterStats, Skill } from "../classes/character-stats";
import { AbilitiyType } from "../classes/ability";
import { PlayerCharacter } from "../classes/character";

export class SkillHelper {
  // Player Stats
  private static _stats: CharacterStats;
  private static _skills: Skill[] = [];
  private static _unlockedSkills: Skill[] = [];
  private static get _unlockedAbilities() {
    return this._unlockedSkills.filter((skill) => {
      return !!skill.isAbility;
    });
  }

  // Html Elements
  private static readonly abilityTreeElement = document.getElementById("abilityTree");
  private static readonly gadgetTreeElement = document.getElementById("gadgetTree");
  private static readonly skillSelect1 = document.getElementById("skillSelect1") as HTMLSelectElement;
  private static readonly skillSelect2 = document.getElementById("skillSelect2") as HTMLSelectElement;
  private static readonly skillSelect3 = document.getElementById("skillSelect3") as HTMLSelectElement;

  static init() {
    // Player stats
    this._stats = PlayerCharacter.instance.stats;
    for (const [, value] of Object.entries(this._stats.skillTree)) {
      this._skills.push(value);
      if ((value as Skill).unlocked) {
        this._unlockedSkills.push(value);
      }
    }

    // Fill Ability and Gadget tree
    this._skills.forEach((skill) => {
      const imageElement = document.createElement("img");
      imageElement.src = skill.image;
      imageElement.title = skill.description;
      imageElement.alt = skill.name;
      imageElement.after("<div>✔️</div>");
      if (!skill.unlocked) {
        imageElement.classList.add("locked");
      }
      if (skill.isAbility) {
        this.abilityTreeElement.appendChild(imageElement);
      } else {
        this.gadgetTreeElement.appendChild(imageElement);
      }
      imageElement.onclick = (event: Event) => {
        if (
          this._stats.skillPoints <= 0 ||
          !confirm(`Spend 1 skill point to learn '${(event.target as HTMLImageElement).alt}'?`)
        )
          return;
        this._stats.unlockSkill(skill);
        this._unlockedSkills.push(skill);
        (event.target as HTMLImageElement).classList.remove("locked");
        (event.target as HTMLImageElement).onclick = null;
        this.addAsOption(skill);
        this.updateOnHover();
      };
    });

    // Fill selection
    [this.skillSelect1, this.skillSelect2, this.skillSelect3].forEach((element, index) => {
      this._unlockedAbilities.forEach((skill) => {
        const option = document.createElement("option");
        option.value = skill.isAbility as string;
        option.innerHTML = skill.name;
        if (skill.isAbility == this._stats.abilitySlots[(index + 1) as 1 | 2 | 3]) {
          option.selected = true;
        }
        element.appendChild(option);
      });
      element.onchange = (event: Event) => {
        this._stats.equipAbility(index + 1, (event.target as HTMLSelectElement).value as AbilitiyType);
        this.updateSelectableOptions();
      };
    });
    this.updateSelectableOptions();
    this.updateOnHover();
  }

  private static addAsOption(skill: Skill) {
    [this.skillSelect1, this.skillSelect2, this.skillSelect3].forEach((element) => {
      const option = document.createElement("option");
      option.value = skill.isAbility as string;
      option.innerHTML = skill.name;
      element.appendChild(option);
    });
  }

  private static updateSelectableOptions() {
    [this.skillSelect1, this.skillSelect2, this.skillSelect3].forEach((element) => {
      Array.from(element.children).forEach((childElement) => {
        const childValue = (childElement as HTMLOptionElement).value;
        if (
          childValue != "Empty" &&
          (childValue == this.skillSelect1.value ||
            childValue == this.skillSelect2.value ||
            childValue == this.skillSelect3.value)
        ) {
          (childElement as HTMLOptionElement).hidden = true;
          (childElement as HTMLOptionElement).disabled = true;
        } else {
          (childElement as HTMLOptionElement).hidden = false;
          (childElement as HTMLOptionElement).disabled = false;
        }
      });
    });
  }

  private static updateOnHover() {
    Array.from(document.getElementsByClassName("locked")).forEach((element) => {
      if (this._stats.skillPoints > 0) {
        element.classList.remove("noSkillPoints");
      } else {
        element.classList.add("noSkillPoints");
      }
    });
  }
}
